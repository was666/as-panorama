# AS-Panorama

vue全景图组件，渲染全景图，切换全景图，全景图内根据经纬度添加标记点；
该组件使用options Api写法，vue2.x与vue3.x都可以使用

demo：[https://was666.gitee.io/as-panorama](https://was666.gitee.io/as-panorama)
## 组件使用示例：

```js
<template>
  <div class="home">
    <viewer
      ref="Viewer"
      :init="init"
      @click="viewerClick"
      @marker="markerClick"
    />
  </div>
</template>

<script setup>
import viewer from '@/components/Viewer.vue'
import { ref, reactive } from 'vue'
const Viewer = ref('Viewer')
const init = reactive({
  // 全景图
  imgurl: require('../assets/bg-0.webp'),
  // 是否显示底部操作栏
  navbar: false,
  // 底部title
  caption: '',
  // 标记点
  marker: [
    {
      id: 'image1',
      image: require('../assets/pin-blue.png'),
      width: 35,
      height: 35,
      longitude: 100,
      latitude: 100,
      anchor: 'bottom center',
      className: 'markers',
      tooltip: 'A circle of radius 30'
    }
  ]
})
const viewerClick = data => {
  console.log(data, '点击非标记位置')
}
const markerClick = data => {
  console.log(data, '点击标记位置')
}
// 切换全景图 imgUrl: 全景图   marker：标记点
const handelChangeViewer = (imgUrl, marker) => {
  Viewer.value.handelChangeViewer(imgUrl, marker)
}
</script>

```

## Project setup

```cmd
npm install
```

### Compiles and hot-reloads for development

```cmd
npm serve
```

### Compiles and minifies for production

```cmd
npm build
```

### Lints and fixes files

```cmd
npm lint
```

### Customize configuration

See [Configuration Reference](https://cli.vuejs.org/config/).
